import json
import os

from wish import jav_info_from_dict, gacha_types
from fetchData import fetchAllDataForGachaType

def calculatePityForGachaType(gacha_type:int, wishes:list) -> int:
  pity = 0
  for wish in wishes:
    if wish.gacha_type == gacha_type:
      pity += 1
    if wish.rank_type == 5:
      return pity
  return pity

def ParseWish(wish_raw_data:dict) -> list:
  list_wish  = wish_raw_data["data"]["list"]
  lt_wishes = [jav_info_from_dict(x) for x in list_wish]
  return lt_wishes

def CalculateStats(gacha_type:int, lt_wishes:list):
  stats = {}
  pity = 0
  for wish in lt_wishes:
    if wish.gacha_type != gacha_type:
      continue

    if wish.rank_type == 5:
      if wish.name in stats:
        stats[wish.name] += [pity+1]
      else:
        stats[wish.name] = [pity+1]
      pity = 0
    else:
      pity += 1

  return stats
  
def GetDataWithFetch(auth_key, force_fetch=False) -> dict:
  # Temporary, only getting the character wish
  if force_fetch or not os.path.isfile("./wish_data.json"):
    data = []
    for gacha_type in gacha_types:
      data += fetchAllDataForGachaType(gacha_type, auth_key)
    
    with open("./wish_data.json", "w") as f:
      json.dump(data, f)
  else:
    with open("./wish_data.json", "r") as f:
      data = json.load(f)
  
  return [jav_info_from_dict(x) for x in data]

# --- Start of main ---

# Get the auth key
with open("./auth_key.txt", "r") as f:
  auth_key = f.readline()
if auth_key == "":
  print("Please put your auth key in auth_key.txt")
  exit(1)

lt_wishes = GetDataWithFetch(auth_key)

# Print all the wishes parsed
# print("\n".join([str(x) for x in lt_wishes]))
# print("")

# Print the pity for the selected banner 5 star
print("")
print("Current pity for the selected banner is %d" % calculatePityForGachaType(301, lt_wishes))
print("")

print("Stats for the selected banner")
pulls_stats = CalculateStats(301, lt_wishes)
avg_pulls = []
for x in pulls_stats:
  print(x)
  for pulls in pulls_stats[x]:
    print("   %d pulls" % pulls)
    avg_pulls.append(pulls)
  print("")

print(f"The pull average is {sum(avg_pulls) / len(avg_pulls):.4g}")