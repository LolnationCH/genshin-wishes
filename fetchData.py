import requests
import json
import datetime
import calendar

def construct_url(gacha_type:int, page:int, auth_key:str, end_id:str) -> str:
  urlBase = "https://hk4e-api-os.mihoyo.com/event/gacha_info/api/getGachaLog?authkey_ver=1&sign_type=2&auth_appid=webview_gacha"
  timestamp = f"&timestamp={calendar.timegm(datetime.datetime.utcnow().utctimetuple())}"
  lang = "&lang=en"
  authKey = f"&authkey={auth_key}"
  gachaType = f"&gacha_type={gacha_type}"
  page = f"&page={page}"
  size = "&size=20"
  end_id = f"&end_id={end_id}"
  return urlBase + timestamp + lang + authKey + gachaType + page + size + end_id

def fetchData(gacha_type:int, page:int, auth_key:str, end_id:str):
  url = construct_url(gacha_type, page, auth_key, end_id)
  try:
    r = requests.get(url)
    data = r.json()
    return data["data"]["list"]
  except:
    return None

def recursive_fetch(gacha_type:int, page:int, auth_key:str, end_id:str, all_data:list):
  data = fetchData(gacha_type, page, auth_key, end_id)
  if data is None or len(data) == 0:
    return all_data
  else:
    all_data.extend(data)
    last_id = data[-1]["id"]
    return recursive_fetch(gacha_type, page+1, auth_key, last_id, all_data)
  
def fetchAllDataForGachaType(gacha_type:int, auth_key:str):
  return recursive_fetch(gacha_type, 1, auth_key, "0", [])