# This code parses date/times, so please
#
#     pip install python-dateutil
#
# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = jav_info_from_dict(json.loads(json_string))

from datetime import datetime
from typing import Any, TypeVar, Type, cast
import dateutil.parser


T = TypeVar("T")

gacha_types = {
  301 : "Character Event Banner",
  302 : "Weapon Banner",
  200 : "Permanent Banner"
}


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


class JavInfo:
    uid: int
    gacha_type: int
    item_id: str
    count: int
    time: datetime
    name: str
    lang: str
    item_type: str
    rank_type: int
    id: str

    def __init__(self, uid: int, gacha_type: int, item_id: str, count: int, time: datetime, name: str, lang: str, item_type: str, rank_type: int, id: str) -> None:
        self.uid = uid
        self.gacha_type = gacha_type
        self.item_id = item_id
        self.count = count
        self.time = time
        self.name = name
        self.lang = lang
        self.item_type = item_type
        self.rank_type = rank_type
        self.id = id

    @staticmethod
    def from_dict(obj: Any) -> 'JavInfo':
        assert isinstance(obj, dict)
        uid = int(from_str(obj.get("uid")))
        gacha_type = int(from_str(obj.get("gacha_type")))
        item_id = from_str(obj.get("item_id"))
        count = int(from_str(obj.get("count")))
        time = from_datetime(obj.get("time"))
        name = from_str(obj.get("name"))
        lang = from_str(obj.get("lang"))
        item_type = from_str(obj.get("item_type"))
        rank_type = int(from_str(obj.get("rank_type")))
        id = from_str(obj.get("id"))
        return JavInfo(uid, gacha_type, item_id, count, time, name, lang, item_type, rank_type, id)

    def to_dict(self) -> dict:
        result: dict = {}
        result["uid"] = from_str(str(self.uid))
        result["gacha_type"] = from_str(str(self.gacha_type))
        result["item_id"] = from_str(self.item_id)
        result["count"] = from_str(str(self.count))
        result["time"] = self.time.isoformat()
        result["name"] = from_str(self.name)
        result["lang"] = from_str(self.lang)
        result["item_type"] = from_str(self.item_type)
        result["rank_type"] = from_str(str(self.rank_type))
        result["id"] = from_str(self.id)
        return result

    def __str__(self) -> str:
        return f'{self.name:<40} : {self.rank_type} stars from {gacha_type[self.gacha_type]} - {self.time}'

    def toCsV(self) -> str:
        return f'{self.uid},{self.gacha_type},{self.name},{self.item_type},{self.rank_type},{self.time}, {self.id}'


def jav_info_from_dict(s: Any) -> JavInfo:
    return JavInfo.from_dict(s)


def jav_info_to_dict(x: JavInfo) -> Any:
    return to_class(JavInfo, x)
