# Genshin Wishes parser

## How to use

### Getting the wishes url

First, you need to have your wishes url, use theses steps :

1. Open Genshin Impact on your PC
1. Then go to the wish history page and wait it to load
1. Go back to Windows
1. In the start menu search for "Powershell" and open "Windows Powershell"
1. Then copy the following code and paste it in the Powershell window

    ```powershell
    iex ((New-Object System.Net.WebClient).DownloadString('https://raw.githubusercontent.com/genshin-wishes/genshin-wishes-getlink/main/global.ps1'));
    ```

1. Hit ENTER then a link will be copied to your clipboard

Once you have the url in your hands, open your web browser.

### Getting the auth token

Look in the url, you should find a `authkey=`, just copy the text until an `&`.

Then make a file named `auth_key.txt`, and put your auth_key there.

### Running the script

1. Make sure you have python install on your computer
2. In a powershell (or cmd) command prompt, type "pip install -r requirements"
3. Then type `python main.py` to launch the script
4. You should now see the pity you have for the selected character banner.

> Note, all your pulls can be found in `whish_data.json`, so you can use them to make sheets, etc..